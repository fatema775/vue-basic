# Basic of Vue JS

## Installation
Import vue.js as a CDN package on the project
````
  <!-- index.html -->
  <script src="https://unpkg.com/vue@next"></script>
````

## Create App
````
  <!-- main.js -->
  const app = Vue.createApp({options object});
````
## Options Object
````
  <!-- main.js -->
  data: function(){
     return{
       key: 'value'
     }
  }

  Shorthand:
    data(){
      return{
      }
    }
````
## Mount App
````
  <!-- index.html -->
  const mountedApp = app.mount('#id')
````
## Dispalay data
````
  <!-- index.html  data: val-->
  <h1>{{ data }}</h1>
````
## Attribute Binding
````
  <!-- main.js url: val, description: val -->
  <img v-bind:src="url" v-bind:alt="description"/>
  Shorthand:
  <img :src="url" :alt="description"/>
````
## Conditional Rendering
````
  <!-- main.js product: val -->
  <!-- index.html -->
  Toggling:
  <p v-show="product">On Sale</p>
  Paired Condition:
  <p v-if="product">In Stock</p>
  <p v-else>Out of Stock</p>
  Conditional Ladder:
  <p v-if="product > 5">In Stock</p>
  <p v-else-if="product <= 5 && product > 0">Almost sold out!</p>
  <p v-else>Out of Stock</p>  
````
## List Rendering
````
  <!-- main.js product_array: ['val', 'val', 'val'] -->
  <!-- index.html -->
  <ul>
    <li v-for="product in product_array">{{ product }}</li>
  </ul>
  <!-- main.js array:[{id: val, var: val}, {id: val, var: val}]
  Use id to help Vue keep track of our list items.
  <!-- index.html -->
  <div v-for="arr in array" :key="arr.id">{{ arr.var }}</div>
  ````
## Event Handling
````
  <!-- main.js var: val -->
  <!-- index.html -->
  <button v-on:click="logic to run">Click Here</button>

  Shorthand:
  <button @click="...">...</button>
  <p @mouseover="...">...</p>
  ````
## Style Binding
````
  <!-- main.js var: val -->
  <!-- index.html -->
  <div :style="{ backgroundColor: var }"></div>
  CSS Property (camelCase backgroundColor) or ('kebab-case' 'background-color')

  Using style object
  <!-- main.js styles: {
    property: 'val',
    property: 'val'
  } -->
  <!-- index.html -->
  <div :style="styles"></div>
  ````
## Class Binding
````
  <!-- style.css: .class_name{ property: val, property: val} -->
  <!-- main.js: var: val -->
  <div :class="{ class_name: !var }"></div>

  Ternary Operator:
  <div :class="{ var ? class_name : ''}"></div>
````
## Computed Properties
````
  <!-- main.js var: val-->
  computed: {
    title() {
      return this.var + ' ' + this.var
    }
  }
````
## Components, Props and Emmiting Events:
````
  <!-- components/com_name.js-->
  app.component('component-name', {
    template: html code,
    data(){...},
    methods: {...},
    component: {...}
  })
  <!-- index.html -->
  <component-name></component-name>
  <script src="components/com_name.js"></script>
  ````
````
  Props:
  <!-- main.js var: val -->
  <!-- index.html -->
  <component-name :var="var"></component-name>
  <!-- com_name.js -->
  props: {
    var: {
      type: Boolean,
      required: true
    }
  },
  template: `
    <p>{{ function_name }}</p>
  `,
  computed: {
    function_name(){
      ...
    }
  }
  ````
````
  Communicate using emitting the event:
  <!-- com_name.js -->
  template: `
    <button @click="function">Click</button>
  `,
  methods: {
    function() {
      this.$emit('emit_fn')
    }
  }
  <!-- index.html -->
  <component-name  :premium="premium" @emit_fn="function_name"></component-name>
  <!-- main.js -->
  methods: {
    function_name() {
      ...
    }
  }
  ````
## Forms and v-model
````
<!-- comp-name.js -->
  template: `
    <form @submit.prevent="onSubmit">
      <input id="name" v-model="name">
      <input type="submit" value="Submit">
    </form>
  `,
  data(){
    return{
      name: ''
    }
  },
  methods: {
   onSubmit() {
     if(this.name === ''){
       alert('Incomplete!')
       return
     }
     let obj = {
       name: this.name
     }
     this.$emit('func', obj)
     this.name = ''
   }
  }
````
````
  <!-- index.html -->
  <script src="components/parent-comp.js"></script>
  <script src="components/comp-name.js"></script>
  <script src="components/new-comp.js"></script>

  <!-- parent-comp.js -->
  template: `
    <component-name v-show="var.length" :prop="var"></component-name>
    <component-name @func="function_name"></component-name>
      `,
  data(){
     return{
       var: []
     }
  },
  methods: {
   function_name(obj){
     this.var.push(obj)
   }
  }
````
````
  <!-- new-comp.js -->
  props: {
    prop: {
      type: Array,
      required: true
    }
  },
  template:`
    html code
  `
````
